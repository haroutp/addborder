﻿using System;

namespace AddBorder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        static string[] addBorder(string[] picture) {
            int len = picture.Length;
            int lenOfArrItem = picture[0].Length;
            string[] ArrToReturn = new string[len + 2];
            
            for(int i = 0; i < ArrToReturn.Length; i++){
                if(i == 0 || i == ArrToReturn.Length - 1){
                    ArrToReturn[i] = new string('*', lenOfArrItem + 2);
                }else{
                    ArrToReturn[i] = '*' + picture[i - 1] + '*';
                }
            }
            return ArrToReturn;
            
        }

    }
}
